package lisp;

import java.util.ArrayList;
import java.util.List;

public class Parser {
    private int pos = 0;
    private List<String> tokens;

    public Parser(List<String> tokens) {
        this.tokens = tokens;
    }

    public Object parse() {
        String token = tokens.get(pos++);
        if (token.equals("(")) {
            List<Object> list = new ArrayList<>();
            while(! tokens.get(pos).equals(")")) {
                list.add(parse());
            }
            pos++;
            return list;
        } else {
            // parse atom
            return parseAtom(token);
        }
    }

    private Object parseAtom(String token) {
        try {
            return Integer.parseInt(token);
        } catch (NumberFormatException e) {
            return token;
        }
    }
}
