package lisp;

import java.util.ArrayList;
import java.util.List;

public class Tokenizer {

    public List<String> tokenize(String input) {
        List<String> tokens = new ArrayList<String>();
        StringBuffer token = new StringBuffer();
        boolean inString = false;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '"') {
                inString = !inString;
                if (! inString) {
                    tokens.add(token.toString());
                    token.setLength(0);
                }
            } else if (Character.isWhitespace(c)) {
                if (inString) {
                    token.append(c);
                } else if (token.length() > 0) {
                    tokens.add(token.toString());
                    token.setLength(0);
                }
            } else if (c == '(' || c == ')') {
                if (inString) {
                    token.append(c);
                } else {
                    if (token.length() > 0) {
                        tokens.add(token.toString());
                        token.setLength(0);
                    }
                    tokens.add(String.valueOf(c));
                }
            } else {
                token.append(c);
            }
        }
        if (token.length() > 0) {
            tokens.add(token.toString());
            token.setLength(0);
        }
        return tokens;
    }
}
